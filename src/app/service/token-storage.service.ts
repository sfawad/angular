import { Injectable } from '@angular/core';
import { User } from 'app/models/user';

const TOKEN_KEY = 'auth-token';
const USER_KEY = 'auth-user';

@Injectable()
export class TokenStorageService {

constructor() { }

signOut(): void {
  window.localStorage.clear();
  window.sessionStorage.clear();
}

public saveToken(token: string, isRememberMe: boolean){
  if(isRememberMe) {

  window.localStorage.removeItem(TOKEN_KEY);
  window.localStorage.setItem(TOKEN_KEY, token);

  } else{
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }
}

public saveUser(user: User, isRememberMe: boolean){
  if(isRememberMe) {

  window.localStorage.removeItem(USER_KEY);
  window.localStorage.setItem(USER_KEY, JSON.stringify(user));
  }
  else {
    window.sessionStorage.removeItem(USER_KEY);
    window.sessionStorage.setItem(USER_KEY, JSON.stringify(user));
  }
}


public getToken(): string {
  return window.localStorage.getItem(TOKEN_KEY);
}



public getUser(): User {
  const user = window.localStorage.getItem(USER_KEY);
  const localuser = window.sessionStorage.getItem(USER_KEY);
  if (user) {
    return JSON.parse(user);
  }
  else{
    return JSON.parse(localuser);
  }

}


}
