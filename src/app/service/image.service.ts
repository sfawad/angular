import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { ImageModel } from '../models/imageModel';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { User } from 'app/models/user';

const httpOptions = {
  headers: new HttpHeaders({ 'content-type': 'application/json' })
};

@Injectable({
  providedIn: 'root',
})
export class ImageService {
  searchOption: any[];
  imagesData: ImageModel[];

  constructor(private http: HttpClient, public datepipe: DatePipe) { }

  private apiServerUrl = environment.baseURL + 'v1/imagesList';
  private uploaddDeleteurl = environment.baseURL + 'v1/upload';
  private baseUrl = environment.baseURL + 'v1/auth/users';
  private updateUser = environment.baseURL + 'v1/auth/update';
  private updateUserEnabled = environment.baseURL + 'v1/auth/updateEnbaled';
  private deleteapi = environment.baseURL + 'v1/auth/delete';
  private resetapi = environment.baseURL + 'v1/auth/forgot/byEmail';
  private changepasswordapi = environment.baseURL + 'v1/auth/reset';
  private checkTokenapi = environment.baseURL + 'v1/auth/verifyToken';

  public uploadImages(userId: number, date: Date, files: File): Observable<HttpEvent<any>> {
    let formData: FormData = new FormData();
    formData.append('fileImage', files);
    formData.append('id', userId.toString());
    formData.append('date', date.toString());
    const req = new HttpRequest('POST', `${this.uploaddDeleteurl}/images`, formData, {
      reportProgress: true,
      responseType: 'text' as 'json'
    });
    return this.http.request(req);
  }

  public getImages(): Observable<ImageModel[]> {
    return this.http.get<ImageModel[]>(`${this.apiServerUrl}/images`);
  }

  public getImagesByUser(userId: number): Observable<ImageModel[]> {
    return this.http.get<ImageModel[]>(`${this.apiServerUrl}/images/${userId}`);
  }

  public deleteImage(id: any) {
    return this.http.delete<boolean>(
      `${this.uploaddDeleteurl}/images/delete/${id}`
    );
  }

  getUser(id: number): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/${id}`);
  }

  getUsersList(): Observable<User[]> {
    return this.http.get<User[]>(`${this.baseUrl}`);
  }

  updateUsers(id: number, username: string, email: string, password: string): Observable<Object> {
    let formData: FormData = new FormData();
    formData.append('email', email);
    formData.append('username', username);
    formData.append('password', password);
    return this.http.put(`${this.updateUser}/${id}`, formData);
  }

  enabledUser(id: number, enabled: boolean): Observable<Object> {
    let formData: FormData = new FormData();
    formData.append('enabled', enabled.toString());
    return this.http.put(`${this.updateUserEnabled}/${id}`, formData);
  }

  deleteUser(id: number): Observable<any> {
    return this.http.delete(`${this.deleteapi}/${id}`, { responseType: 'text' });
  }

  forgotPassword(email: string): Observable<any> {
    return this.http.put(`${this.resetapi}/${email}`, httpOptions);
  }

  changePassword(token: string, password: string): Observable<User> {
    let formData: FormData = new FormData();
    formData.append('token', token);
    formData.append('password', password);
    return this.http.put<User>(`${this.changepasswordapi}`, formData);
  }

  checkToken(token: string): Observable<any> {
    return this.http.get(`${this.checkTokenapi}/${token}`);
  }
}
