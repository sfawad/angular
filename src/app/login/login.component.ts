import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'app/service/auth.service';
import { TokenStorageService } from 'app/service/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form: any = {};
  errorMessage = '';
  isLoggedIn = false;
  isLoginFailed = false;
  isRememberMe = false;
  roles: string[] = [];
  Formdata:any = {};

  constructor(private authService: AuthService,
    private router: Router,
    private tokenStorage: TokenStorageService) {

      if (this.tokenStorage.getUser()) {
        this.router.navigate(['/main/userdashboard']);
    }
      }

  ngOnInit() {
  }

  onSubmit(): void {
    const { username, password} = this.form;

    this.authService.login(username, password).subscribe(

      data => {
        this.tokenStorage.saveToken(data.accessToken,this.isRememberMe );
        this.tokenStorage.saveUser(data, this.isRememberMe);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.router.navigate(['/main/userdashboard']);

      },
      err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    );
  }

  onNew(): void {
    this.router.navigate(['signup']);
  }

  reset(): void {
    this.router.navigate(['/login/resetPassword']);
  }
}
