import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'app/models/user';
import { ImageService } from 'app/service/image.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

    form = new FormGroup({
    email: new FormControl('', [ Validators.required, Validators.email]) });
    errorMessage = '';
    submitted = false;
    succesMessage = '';
    isSubmitFailed = false

    constructor(private userService: ImageService, private router: Router) { }

    ngOnInit() {
    }

    onSubmit() {
    if (this.form.invalid) {
        return;
    }

    const email = this.form.get("email").value
    this.userService.forgotPassword(email).subscribe(
      data => {
        this.isSubmitFailed = false
        this.submitted = true;
        this.router.navigate(['changePassword']);
        this.succesMessage = data

      },
      err => {
        this.errorMessage = err.error.message;
        this.isSubmitFailed = true;
      }
    );
}
}
