import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ImageService } from 'app/service/image.service';
import { CustomValidators } from 'app/_helpers/custom-validator';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  passwordForm: FormGroup;
  submitted = false;
  errorMessage = '';
  errorMessage2 = '';
  validToken= false
  isSubmitFailed = false
  valid = false

  constructor(private fb: FormBuilder, private userService: ImageService, private router: Router) {}

  ngOnInit() {
    this.passwordForm = this.fb.group(
      {
        token: [
          '',
          Validators.compose([Validators.required])
        ],
        password: [
          null,
          Validators.compose([
            Validators.required,
            CustomValidators.patternValidator(/\d/, {
              hasNumber: true
            }),
            CustomValidators.patternValidator(/[a-z]/, {
              hasSmallLetter: true
            }),
            CustomValidators.patternValidator(/[A-Z]/, {
              hasCapitalLetter: true
            }),
            CustomValidators.patternValidator(
              /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
              {
                hasSpecialCharacters: true
              }
            ),
            Validators.minLength(8)
          ])
        ],
        confirmPassword: [null, Validators.compose([Validators.required])]
      },
      {
        validator: CustomValidators.passwordMatchValidator
      }
    );
  }

  get f() { return this.passwordForm.controls; }

  onSubmit() {
    const token = this.passwordForm.get("token").value;
    const password = this.passwordForm.get("password").value;
      this.submitted = true;
      if (this.passwordForm.invalid) {
          return;
      }
    this.userService.changePassword(token,password).subscribe(
      data => {
        this.submitted = true;
        this.isSubmitFailed = false;
        this.router.navigate(['successreset']);
      },
      err => {
        this.errorMessage = err.error.message;
        this.isSubmitFailed = true;
      }
    );
  }

  checkValid(){
    const token = this.passwordForm.get("token").value;
    this.userService.checkToken(token).subscribe(
      data => {
        this.submitted = true;
        this.isSubmitFailed = false;
       this.validToken=!data.isValid
        this.valid = data.isValid
        if(this.validToken)
        this.errorMessage2="Token is Invalid!"
      },
      err => {
        this.errorMessage = err.error.message;
        this.validToken = false
      }
    );
  }
}
