import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'app/service/auth.service';

class CustomValidators {
  static passwordsMatch (control: AbstractControl): ValidationErrors {
    const password = control.get('password').value;
    const confirmPassword = control.get('confirmPassword').value;
    if((password === confirmPassword) && (password !== null && confirmPassword !== null)) {
      return null;
    } else {
      return {passwordsNotMatching: true};
    }
  }
}

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css'],
})
export class RegisterUserComponent implements OnInit {

  isSuccessful = false;
  isSignUpFailed = false;
  errorMessage = '';

 form = new FormGroup({
  username: new FormControl('', [Validators.required, Validators.minLength(4)]),
  email: new FormControl('',[Validators.required, Validators.email]),
  firstName: new FormControl('',[Validators.required]),
  lastName: new FormControl('',[Validators.required]),
  dob: new FormControl('',[Validators.required]),
  password: new FormControl('',[
        Validators.required,
        Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{7,}')
        ]),
  confirmPassword: new FormControl('', [
        Validators.required,]) },
        { validators: CustomValidators.passwordsMatch });

  constructor( private authService: AuthService,
    private router: Router
    ) { }

  ngOnInit() {}

  onSubmit(): void {
    if(this.form.valid){
      const username =  this.form.get("username").value
      const email =  this.form.get("email").value
      const password =  this.form.get("password").value
      const firstName =  this.form.get("firstName").value
      const lastName =  this.form.get("lastName").value
      const dob =  this.form.get("dob").value
      this.authService.register(username, email, password, firstName, lastName, dob).subscribe(
        data => {
          this.isSuccessful = true;
        },
        err => {
          this.errorMessage = err.error.message;
          this.isSignUpFailed = true;
        }
      );
    }
  }

  login(): void {
    this.router.navigate(['login']);
  }
}
