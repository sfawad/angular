import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './sidebar/dashboard.component';
import { LoginComponent } from './login/login.component';
import { ShowImagesComponent } from './manage-uploaded/image-details-view/show-images.component';
import { AllImagesComponent } from './manage-uploaded/list-view-images/all-images.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { UploadImageComponent } from './upload/upload-image.component';
import { LoginLayoutComponent } from './login-layout/login-layout.component';
import { AuthGuard } from './service/auth.guard';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { UpdateUiComponent } from './manage-users/update-ui/update-ui.component';
import { AdminSignupComponent } from './admin-signup/admin-signup.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SuccesresetComponent } from './change-password/succesreset/succesreset.component';
import { UserdashboardComponent } from './userdashboard/userdashboard.component';
import { CalendarComponent } from './userdashboard/calendar/calendar.component';

const routes: Routes = [

  { path: 'successreset', component: SuccesresetComponent, data: { title: 'Security Updated ' } },
  { path: 'changePassword', component: ChangePasswordComponent, data: { title: 'ChangedPassword ' } },
  { path: 'signup', component: RegisterUserComponent, data: { title: 'SignUp ' } },
  { path: '', redirectTo: "login", data: { title: 'Login ' }, pathMatch: "full" },
  {
    path: 'login', component: LoginLayoutComponent, data: { title: 'LoginLayout ' },
    children: [
      { path: '', component: LoginComponent },
      { path: 'resetPassword', component: ResetPasswordComponent }
    ]
  },
  {
    path: 'main', component: DashboardComponent, canActivate: [AuthGuard],
    children: [
      { path: 'userdashboard', component: UserdashboardComponent, data: { title: 'Dashboard' }, canActivate: [AuthGuard] },
      { path: 'upload', component: UploadImageComponent, canActivate: [AuthGuard] },
      { path: 'showImages', component: ShowImagesComponent , canActivate: [AuthGuard]},
      { path: 'allImages', component: AllImagesComponent, canActivate: [AuthGuard] },
      { path: 'users', component: ManageUsersComponent, data: { title: 'Users ' }, canActivate: [AuthGuard] },
      { path: 'admin/signup', component: AdminSignupComponent, data: { title: 'AdminSignUp ' }, canActivate: [AuthGuard] },
      { path: 'updates/:id', component: UpdateUiComponent, data: { title: 'Update ' }, canActivate: [AuthGuard] },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
