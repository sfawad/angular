FROM node:14.18.0-alpine3.14 as build-step
WORKDIR /app
COPY package*.json /app
### ADD package*.json /app
COPY . /app
#RUN npm install
#RUN npm link @angular/cli
### RUN npm run build --prod
###RUN npm run build
#RUN ng build


#FROM nginx:1.17.1-alpine
FROM nginx:1.21-alpine

RUN rm -rf /usr/share/nginx/html/*
##COPY  --from=build-step /app/nginx/*  /etc/nginx/conf.d/default.conf

#COPY --from=build-step /app/dist/ng-docker-example /usr/share/nginx/html
COPY --from=build-step /app/dist/* /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]